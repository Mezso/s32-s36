const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require('../auth')

router.post("/",auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true){
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("Invalid credential")
	}
	
})


router.get('/all',(req,res) =>{
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})

router.get('/',(req,res)=>{
	courseController.getAllActive().then(resultFromController =>{
		res.send(resultFromController)
	})
})
router.get('/:courseId',(req,res)=>{
	console.log(req.params.courseId)
	courseController.getCourse(req.params).then(resultFromController=>res.send(resultFromController))
})


router.put('/:courseId',auth.verify, (req,res) =>{
	courseController.updateCourse(req.params,req.body).then(resultFromController=> res.send(resultFromController))
})

router.put('/:courseId/archieve',auth.verify,(req,res) =>{
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true){
		courseController.ArchievedCourse(req.params,req.body).then(resultFromController=> res.send(resultFromController))
	} else {
		res.send('invalid credential')
	}
	
})

module.exports = router;
