const Course = require("../models/Course");


module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({	
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((course, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
}

module.exports.getAllCourses = () => {
	return Course.find({}).then(result =>{
		return result
	})
}

module.exports.getAllActive = () =>{
	return Course.find({isActive: true}).then(result =>{
	return result;
	})
}

module.exports.getCourse = (reqParams) =>{
	return Course.findById(reqParams.courseId).then(result => {
		return result
	})
}

//Update a course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	//Syntax:
		//findByIdAndUpdate(document, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
	.then((course, error) =>{
		if(error){
			return false
		} else {
			return true
		}
	})
}

module.exports.ArchievedCourse = (reqParams,reqBody) =>{
	
	
		let ArchieveCourse = {
			isActive : reqBody.isActive
		}
		return Course.findByIdAndUpdate(reqParams.courseId,ArchieveCourse).then((course,error)=>{
			if(error){
				return false
			} else {
				return true
			}
		})
	
	
}
